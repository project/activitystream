<?php

/**
 * Invoke a node hook.
 *
 * @param $module
 *   The name of your module 
 *
 * @param &$user
 *   A user object containing the user that you're saving account info 
 *   for.
 * 
 * @param $params
 *   An array containing the user's account info. All fields are
 *   optional, but if you don't send anything, nothing will be saved
 *   Fields:
 *      userid: The user's account id on the remote site
 *      password: The user's password on the remote site. Don't
 *          ask for and store the users's password unless it's
 *          neccessary. You can often get read access to the 
 *          site's data with just their username.
 *      feed: An RSS or Atom feed URL containing that user's
 *          stream from the remote site
 * @return
 *   True if the information was saved
 */
function activitystream_save_account($module, &$user, $params) {
  if (count($params) == 0) {
    return false;
  }
  $result = db_query('DELETE FROM {activitystream_accounts} WHERE module = \'%s\' and uid = %d', $module, $user->uid);
  if (empty($params['userid']) && empty($params['password']) && empty($params['feed'])) {
    return;
  }
  // Save multiline feed fields each to a different feed
  $arrFeeds = split("\n", $params['feed']);
  foreach ($arrFeeds as $url) {
    $params['feed'] = $url;
    $result = db_query('INSERT INTO {activitystream_accounts} (module, uid, userid, password, feed) VALUES (\'%s\', %d, \'%s\', \'%s\', \'%s\')', $module, $user->uid, $params['userid'], $params['password'], $params['feed']);    
  }
  return true;
}

function activitystream_menu() {
  $items = array();

  $items[] = array(
    'path' => 'admin/settings/activitystream',
    'title' => t('Activity Stream'),
    'description' => t('Administer settings for activity feeds'),
    'callback' => 'drupal_get_form',
    'callback arguments' => 'activitystream_settings',
    'access' => user_access('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items[] = array('path' => 'stream',
     'title' => t('Activity Stream'),
    'callback' => 'activitystream_page',
    'access' => user_access('access content'),
    'type' => MENU_CALLBACK
  );

  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function activitystream_node_info() {
  return array(
    'activitystream' => array(
      'name'        => t('Activity Stream Item'),
      'module'      => 'activitystream',
      'description' => t('A node type to contain items from your activity stream. You shouldn\'t create these nodes directly.'),
      'help'        => t('A node type to contain items from your activity stream. You shouldn\'t create these nodes directly.'),
      'body_label'  => t('Body'),
    )
  );
}

/**
 * Implementation of hook_nodeapi(). 
 * When a node is deleted, also delete the associated record in the stream table.
 */
function activitystream_nodeapi(&$node, $op, $arg = 0) {
  switch ($op) {
    case 'delete':
      $result = db_query('DELETE FROM {activitystream} WHERE nid = %d', $node->nid);
  }
}

/**
 * Implementation of hook_form().
 * Build the node edit form for a Activity Stream node.
 */
function activitystream_form(&$node) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5
  );
  $form['body_field'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#default_value' => $node->body,
    '#required' => FALSE
  );
 
  return $form;
}

/*
 * Calls an API hook that allows activitystream modules to add admin form
 * items to the Activity Stream settings page.
 */
function activitystream_form_alter($form_id, &$form) {
  unset($form['Activity Stream']);
  if ($form_id == 'activitystream_settings') {
    foreach (module_implements('activitystream_admin') as $name) {
      $function = $name .'_activitystream_admin';
        $elements = $function();
      foreach ($elements as $key => $value) {
        $form[$key] = $value;
      }
    }
  }
}

/**
 * Implementation of hook_user()
 */
function activitystream_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'categories':
      return array('activitystream' => array('name' => 'activitystream', 'title' => variable_get('activitystream_title', 'Activity Stream'), 'weight' => 2));
      break;
    case 'form':
      if ($category == 'activitystream') {
        foreach (module_implements('activitystream_settings') as $name) {
          $function = $name .'_activitystream_settings';
            $elements = $function($edit);
          foreach ($elements as $key => $value) {
            $form[$key] = $value;
          }
        }
      }
      return $form;
      break;
    case 'update':
      foreach (module_implements('activitystream_settings') as $name) {
        $arrDetails = array();
        $arrDetails['userid'] = array_key_exists($name . '_userid', $edit) ? $edit[$name . '_userid'] : null;
        $arrDetails['password'] = array_key_exists($name . '_password', $edit) ? $edit[$name . '_password'] : null;
        $arrDetails['feed'] = array_key_exists($name . '_feed', $edit) ? $edit[$name . '_feed'] : null;
        activitystream_save_account($name, $account, $arrDetails);        
      }
      break;
    case 'view':
      $items = _activitystream_get_activity($account);
      $title = variable_get('activitystream_title', 'Activity Stream');
      $output = theme('activitystream', $items);
      if (!empty($output)) {
        return array($title => array(array('value' => $output, 'class' => 'user')));
      }
      break;
  }
}

function activitystream_page($uid = 0) {
  drupal_add_css(drupal_get_path('module', 'activitystream') .'/activitystream.css');
  $title = variable_get('activitystream_title', 'Activity Stream');
  if ($uid) {
    $user = activitystream_user_load($uid);
    drupal_set_title(check_plain($user->name .'\'s ' . $title));
    $items = _activitystream_get_activity($user);
    drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Activity Stream'), 'stream')));
  } 
  else {
    drupal_set_title(check_plain('All user\'s ' . $title . 's'));
    $items = _activitystream_get_activity();    
  }
  $output = theme('activitystream', $items);
  
  // Breadcrumb navigation
  $breadcrumb[] = array('path' => 'stream', 'title' => t($title));
  drupal_set_breadcrumb($breadcrumb);
  $pager = theme('pager', NULL, 15, 0);
  if (!empty($pager)) {
    $output .= $pager;
  }
  
  return $output;
}

/*
  hook_view
*/
function activitystream_view($node, $teaser = FALSE, $page = FALSE) {
  $user = activitystream_user_load($node->uid);
  $title = variable_get('activitystream_title', 'Activity Stream');
  $node->body = check_markup($node->body, $node->format, FALSE);

  $result = db_query('SELECT s.module, s.link, s.data, s.nid FROM {activitystream} s WHERE s.nid=%d LIMIT 1', $node->nid);      
  $action = db_fetch_object($result);
  $node->body .= theme('activitystream_view', $action);

  $node->content['body'] = array(
    '#value' => $node->body,
    '#weight' => 0,
  );

  if ($page) {
    // Breadcrumb navigation
    $breadcrumb[] = array('path' => 'stream', 'title' => t($title));
    $breadcrumb[] = array('path' => 'stream/'.$user->uid, 'title' => check_plain($user->name) .'\'s ' . $title);
    $breadcrumb[] = array('path' => 'node/'. $node->nid);
    menu_set_location($breadcrumb);
  }
  return $node;
}

function activitystream_get_activity($user = null, $show_buddies = false, $count = null) {
  $items = _activitystream_get_activity($user, $show_buddies, $count);
  return theme('activitystream',$items);
}

function _activitystream_get_activity($user = null, $show_buddies = false, $count = null) {
  if ($count == null) {
    $count = variable_get('default_nodes_main', 10);
  }
  if (module_exists('buddylist') && $show_buddies) {
    $buddies = db_query('SELECT bl.buddy as bid FROM {buddylist} bl WHERE bl.uid = %d',$user->uid);
    // Since a number of items might be from the same buddy, load the users now and stuff them
    // into an array for later. This avoids loading the same user multiple times.
    while ($buddy = db_fetch_object($buddies)) {
      $objBuddy = activitystream_user_load($buddy->bid);
      }
    if (count($arrUsers) == 0) {
      // No buddies
      return;
    }

    $query = 'SELECT n.title, n.nid, s.module, s.link, s.data, n.created FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status =1 AND n.uid in ('.join(',',array_keys($arrUsers)).') ORDER BY n.created DESC';
    $countquery = 'SELECT count(*) FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status =1 AND n.uid in ('.join(',',array_keys($arrUsers)).')';
  } elseif ($user) {
    $query = "SELECT n.title, n.nid, s.module, s.link, s.data, n.created FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status =1 AND n.uid = " . $user->uid . " ORDER BY n.created DESC";  
    $countquery = "SELECT count(*) FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status =1 AND n.uid = " . $user->uid;  
  } else {
    $query = 'SELECT n.title, n.nid, s.module, s.link, s.data, n.created FROM {activitystream} s LEFT JOIN {node} n on s.nid=n.nid WHERE n.status = 1 ORDER BY n.created DESC';      
    $countquery = 'SELECT count(*) FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status = 1';      
  }
  $datehead = '';
  $items = array();
  $stream = pager_query($query, $count, 0, $countquery);
  while ($action = db_fetch_object($stream)) {
    if (date('Ymd', $action->created) != $datehead) {
      $datehead = date('Ymd', $action->created);
      $items[] = theme('activitystream_header', $action);
    }
    
    if (function_exists('theme_' . $action->module .'_item')) {
      $theme_function = $action->module .'_item';
    } 
    else {
      $theme_function = 'activitystream_item';
    }
    $items[] = theme($theme_function,$action);      
  }
  return $items;
}

function theme_activitystream_header(&$action) {
  return '<h3 class="datehead">' . format_date($action->created, 'medium') . '</h3>'; 
}

function theme_activitystream_item($action) {
  $node = node_load($action->nid);
  $date = theme('activitystream_date',$node->created);
  $user = activitystream_user_load($node->uid);
  $name = theme('activitystream_username',$user);
  if (function_exists('theme_'.$action->module .'_icon')) {
    $theme_function = $action->module .'_icon';
  } 
  else {
    $theme_function = 'activitystream_icon';
  }
  return '<span class="activitystream-item">' . theme($theme_function) . " <span>$name " . l($node->title, 'node/'. $node->nid) . " <span class=\"activitystream-created\">$date</span></span>" . l('#', 'node/' . $node->nid, array('class' => 'permalink')) . "</span>\n";
}


function theme_activitystream($items) {
  drupal_add_css(drupal_get_path('module', 'activitystream') .'/activitystream.css');
  if (!count($items)) {
    $items = array(t('There are no activities to show.'));
  }
  return '<div id="activitystream">' . "\n" . theme('item_list',$items) . "\n</div>";
}

function theme_activitystream_date($date) {
  $date = format_date($date,'custom','g:ia');
  return $date;
}

function theme_activitystream_username($user) {
  $arrNames = split(' ',$user->name);
  if (user_access('access user profiles')) {
    return l($arrNames[0],'user/' . $user->uid);
  } else {
    return $arrNames[0];
  }
}

function theme_activitystream_view($activity) {
  $node = node_load($activity->nid);
  $icon_theme = $activity->module.'_icon';
  $return = '<p class="activitystream-original">See original: ';
  $return .= theme($icon_theme,$activity->data) . ' ' . l($node->title, $activity->link);
  $return .= '</p>';
  return $return;
}


function activitystream_cron() {
  $result = db_query('SELECT uid, userid, password, feed, module from {activitystream_accounts}');
  while ($user = db_fetch_object($result)) {
    activitystream_invoke_streamapi($user);
  }  
}


/**
 * Implementation of hook_settings()
 */
function activitystream_settings() {
  $form['activitystream_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('activitystream_title', 'Activity Stream'),
    '#description' => t('The title of the Activiy Stream in blocks and User Profiles.')
  );
  
  return system_settings_form($form);
}


function activitystream_invoke_streamapi($user) {
  $return = array();
  if (module_hook($user->module, 'streamapi')) {
    $items = array();
    $function = $user->module .'_streamapi';
    $items = $function($user);
    if (is_array($items)) {
      foreach ($items as $activity) {
        $return[] = _activitystream_save($activity, $user, $user->module);    
      }    
    }
  }
  return $return;
}

/**
 * Shortcut the user_load function if we already have loaded this user.
 */
function activitystream_user_load($uid) {
  static $arr_users;
  if (!is_array($arr_users)) {
    $arr_users = array();
  }
  if ($arr_users[$uid]) {
    return $arr_users[$uid];
  } else {
    $user = user_load(array('uid' => $uid));
    $arr_users[$uid] = $user;
    return $user;    
  }
}

function _activitystream_save($activity, $user, $name) {
  // Find old-style activity guids. We changed to include the uid in the
  // guid so that we could have multiple users with the same activity.
  // But this meant that we'd duplicate items. So if we have an old-style
  // guid, delete the item and re-create it.
  // This check will be removed in a future version.
  if (empty($activity['guid']) || !array_key_exists('guid',$activity)) {
    $activity['guid'] = md5($name,$activity['link']);
  } 
  else {
    $activity['guid'] = md5($activity['guid']);    
  }
  $node = db_fetch_object(db_query("SELECT nid FROM {activitystream} WHERE guid = '%s'", $activity['guid']));
  if ($node->nid) {
    node_delete($node->nid);
  }
  // End old-style guid check.
  
  if (empty($activity['guid']) || !array_key_exists('guid',$activity)) {
    $activity['guid'] = md5($name.$activity['link'].$user->uid);
  } 
  else {
    $activity['guid'] = md5($activity['guid'].$user->uid);    
  }  
  if (empty($activity['link'])) {
    $activity['link'] = '';
  }
  if (empty($activity['data'])) {
    $activity['data'] = '';
  }
  $node = db_fetch_object(db_query("SELECT nid, changed FROM {activitystream} WHERE guid = '%s'",$activity['guid']));
  if ($node->nid) {
    $changed = $node->changed;
    $node = node_load($node->nid);
    if ($changed != $node->changed) {
      return $node->nid;
    }
    $new = false;
  } 
  else {
    $node = new stdClass();  
    $new = true;
    $options = variable_get('node_options_activitystream', FALSE);
    if (is_array($options)) {
      $node->status = in_array('status', $options) ? 1 : 0;
      $node->promote = in_array('promote', $options) ? 1 : 0;
      $node->sticky = in_array('sticky', $options) ? 1 : 0;
    }
    else {
      $node->status = 1;
    }
  }
  // Has the source changed? If not, we don't want to update the node
  $source_changed = ($node->title == $activity['title'] && $node->body == $activity['body']) ? false : true;
  if ($source_changed || $new) {
    // Only save if if the source has changed or this is a new item
    $node->title = $activity['title'];
    $node->body = $activity['body'];
    $node->created = $activity['timestamp'];
    $node->uid = $user->uid;
    $node->type = 'activitystream';
    if ($new) {
      node_object_prepare($node);
      node_save($node);
      $actions = db_query('INSERT into {activitystream} (nid, module, guid, link, data, changed) VALUES (%d,\'%s\', \'%s\',\'%s\',\'%s\', %d)', $node->nid, $name, $activity['guid'], $activity['link'], $activity['data'], $node->changed);    
      watchdog('activitystream', t('Added %title from %name', array('%title' => $node->title, '%name' => $name)));    
    } else {
      node_save($node);
      watchdog('activitystream', t('Updated %title from %name', array('%title' => $node->title, '%name' => $name)));    
      $actions = db_query('UPDATE {activitystream} SET changed = %d', $node->changed);
    }
  }
  return $node->nid;
}

function activitystream_views_tables() {
  $tables = array();
  $tables['activitystream'] = array(
    'name' => 'activitystream',
    'provider' => 'internal',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'table' => 'activitystream',
        'field' => 'nid'
      )
    ),
    'fields' => array(
      'source' => array(
        'name' => t('Activity Stream: Source'),
        'handler' => array(
          'views_handler_field_profile_default' => t('Source')
        )
      ),
      'data' => array(
        'name' => t('Activity Stream: Icon'),
        'handler' => array(
          'activitystream_views_handler_field_icon' => t('Icon')
        )
      ),
      'link' => array(
        'name' => t('Activity Stream: Link'),
        'handler' => array(
          'activitystream_views_handler_field_link' => t('Link')
        )
      )
    ),
    'filters' => array(
      'module' => array(
        'name' => t('Activity Stream: Type'),
        'list' => '_activitystream_views_handler_filter_type',
        'list-type' => 'list',
        'operator' => 'views_handler_operator_or',
        'value-type' => 'array',
        'help' => t('Include or exclude streams of the selected types.')
      )
    )
  );
  
  return $tables;
}

function _activitystream_views_handler_filter_type() {
  $sources = array();
  $module_list = module_rebuild_cache();
  $implementing_modules = module_implements('streamapi');
  foreach ($implementing_modules as $module_name) {
    $sources[$module_name] = $module_list[$module_name]->info['name'];
  }
  return $sources;
}

function activitystream_views_handler_field_link($fieldinfo, $fielddata, $value, $data) {
  return l($value, $value);
}

function activitystream_views_handler_field_icon($fieldinfo, $fielddata, $value, $data) {
  $stream = unserialize($value);
  $output = sprintf('<img src="%s"/>', $stream['favicon']);
  return $output;
}

function activitystream_views_style_plugins() {
  return array(
    'activitystream' => array(
      'name' => t('Activity Stream'),
      'theme' => 'activitystream_views_view'
    )
  );
}

function theme_activitystream_views_view($view, $nodes, $type) {
  $datehead = '';
  $items = array();
  foreach ($nodes as $node) {
    $action = node_load($node->nid);
    if (date('Ymd', $action->created) != $datehead) {
      $datehead = date('Ymd', $action->created);
      $items[] = theme('activitystream_header', $action);
    }
    
    if (function_exists('theme_' . $action->module .'_item')) {
      $theme_function = $action->module .'_item';
    } 
    else {
      $theme_function = 'activitystream_item';
    }
    $items[] = theme($theme_function, $action);      
  }
  return theme('activitystream', $items);
}

function activitystream_views_default_views() {
  $view = new stdClass();
  $view->name = 'activity_stream_delicious';
  $view->description = t('Displays the activity stream for Delicious only.');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Delicious Activity';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'activitystream';
  $view->url = 'users/$arg/delicious';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '50';
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'uid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream',
      ),
    ),
    array (
      'tablename' => 'activitystream',
      'field' => 'module',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream_delicious',
      ),
    ),
  );
  $view->exposed_filter = array();
  $view->requires = array('node', 'activitystream');
  $views[$view->name] = $view;

  $view = new stdClass();
  $view->name = 'activity_stream_digg';
  $view->description = t('Displays the activity stream for Digg only.');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Digg Activity';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'activitystream';
  $view->url = 'users/$arg/digg';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->sort = array();
  $view->argument = array (
    array (
      'type' => 'uid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream',
      ),
    ),
    array (
      'tablename' => 'activitystream',
      'field' => 'module',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream_digg',
      ),
    ),
  );
  $view->exposed_filter = array();
  $view->requires = array('node', 'activitystream');
  $views[$view->name] = $view;

  $view = new stdClass();
  $view->name = 'activity_stream_twitter';
  $view->description = t('Displays the activity stream for Twitter only.');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Twitter Activity';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'activitystream';
  $view->url = 'users/$arg/twitter';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->sort = array();
  $view->argument = array (
    array (
      'type' => 'uid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => '',
      'handler' => 'views_handler_field_since',
    ),
    array (
      'tablename' => 'activitystream',
      'field' => 'data',
      'label' => '',
      'handler' => 'activitystream_views_handler_field_feed_icon',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream',
      ),
    ),
    array (
      'tablename' => 'activitystream',
      'field' => 'module',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'activitystream_twitter',
      ),
    ),
  );
  $view->exposed_filter = array();
  $view->requires = array('node', 'activitystream');
  $views[$view->name] = $view;

  return $views;
}